This is an example of a crate that can break at a distance when used as
a dependency after being "normalized" into a `*.crate` package with `cargo
package` or used with `cargo vendor`.
